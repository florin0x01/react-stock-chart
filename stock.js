var React = require('react');
var createReactClass = require('create-react-class');
var Bar = require('react-chartjs-2').Bar
var axios = require('axios')
var Select = require('react-select').default
var DatePicker = require('react-datepicker')
var moment = require('moment')

var Stock = createReactClass({
    getInitialState: function() {
        console.log("Initial state. PROPS");
        console.log(this.props);
        return {
            chartData: this.props.chartData,
            selectableOptions: this.props.selectableOptions,
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            company: this.props.company,
            percentages: this.props.percentages,
            estimating: false
        }
    },

    _doEstimatePrice: function(numForecastDays, curState) {
        //gets row on row diff change between current day and previous day
        //row[t] = (y[t] – y[t-1]) / y[t-1]        
       estValues = [];
       avg = 0.0;

       let myDataset = curState.chartData.datasets[0].data;

       startPoint = myDataset.length /3;

       startPoint = Math.floor(startPoint);             
       console.log("start point: ", startPoint);

       for(t=startPoint; t < myDataset.length; t++) {
           let curElem = myDataset[t];
           let prevElem = myDataset[t-1];
           let newVal = (curElem-prevElem)
           estValues.push(newVal)
           avg += newVal / myDataset.length;
       }
       avg = avg / myDataset.length;

       for(t=1; t < myDataset.length; t++) {
            estValues[t-1] = estValues[t-1] + avg;
       }

       console.log("estimated vals")
       console.log(estValues);

       idx=0;
       let lastValue = curState.chartData.datasets[0].data[curState.chartData.datasets[0].data.length-1];
       while(numForecastDays--) {
            lastValue = lastValue + estValues[idx];
            console.log("Last val: ", lastValue);
            curState.chartData.datasets[0].data.push(lastValue + estValues[idx])
            idx++;
            let newDateStr = moment().businessAdd(idx, 'days').format("YYYY-MM-DD");
          //  console.log("New date: ", newDateStr) ;
            curState.chartData.labels.push(newDateStr);                        
       }
       return curState;
    },

    _regulateChartData: function() {
        let curState = Object.assign({}, this.state);

        curState.chartData.labels = []
        curState.chartData.datasets[0].data = []
        curState.chartData.datasets[0].label = curState.company + ' share price '

        var apiKey = 'ZvxJtvoqPKVDNQz3CwRQ'
        url = 'https://www.quandl.com/api/v3/datasets/WIKI/' + curState.company + '/data.json?api_key=' + apiKey 
            + '&start_date=' + curState.startDate + '&end_date=' + curState.endDate + '&column_index=4&order=asc'
    
        console.log("URL: ", url);    
        axios.get(url)
        .then(chartDataPoints => {
            console.log(chartDataPoints);
            console.log("Got data p ", chartDataPoints.data.dataset_data.data.length)
            for (elem=0; elem < chartDataPoints.data.dataset_data.data.length; elem++) {
                curState.chartData.labels.push(chartDataPoints.data.dataset_data.data[elem][0]);
                curState.chartData.datasets[0].data.push(chartDataPoints.data.dataset_data.data[elem][1]);
            }
            if (this.state.estimating === true) {
                let difDaysFromPresent =  30;
                curState = this._doEstimatePrice(30, curState);
            }
            console.log(this.props.chartData);            
            this.setState(curState)                  
        });
    },
    
    componentDidMount: function() {
        console.log("Component did mount")
        this._regulateChartData()
    },

    componentDidUpdate: function(prevProps, prevState) {
       console.log("Component did update")
       console.log(prevProps)
       console.log(prevState)
       if ( (prevState.company != this.state.company) || 
            (prevState.endDate != this.state.endDate) || 
            (prevState.startDate != this.state.startDate) ) 
        {
            this._regulateChartData()          
       }
    },

    _logChange: function(pt) {
        console.log("_logChange: ", pt);
        this.setState( {company: pt.value} );
      //  this._regulateChartData() 
    },

    _handleDateChange1: function(d) {
        console.log("_handleDateChange1: ");
        if (moment(d) >= moment(this.state.endDate)) {
            alert("Can't set start > end")
            return;
        }
        this.setState({startDate: moment(d).format("YYYY-MM-DD")})
    },

    _handleDateChange2: function(d) {
        console.log("_handleDateChange2: ");
        if (moment(d) <= moment(this.state.startDate)) {
            alert("Can't set end <= start")
            return;
        }
        let est = false;
        if (moment(d) > moment()) {
            console.log("Estimating");
            est = true;
        }
        this.setState({endDate: moment(d).format("YYYY-MM-DD"), estimating:est})
    },

    render: function() {
        var divStyle = {
            height: this.props.height,
            width: this.props.width
        }
        return (
            <div style={divStyle}> 
                Company: <Select
                    name="selectStock"
                    value={this.state.company}
                    options={this.props.selectableOptions}
                    onChange={this._logChange}
                    placeholder="Select company"
                />
                StartDate: <DatePicker
                    selected={moment(this.state.startDate)}
                    onSelect={this._handleDateChange1}
                />
                EndDate: <DatePicker
                    selected={moment(this.state.endDate)}
                    onSelect={this._handleDateChange2}
                />
              <Bar 
                    data={this.state.chartData} 
                    company={this.state.company} 
                    options={this.props.chartOptions} 
                    width={this.props.width} 
                    height={this.props.height}
                 />
            </div>
        )
    }
});

Stock.defaultProps = {
    chartData: {
        labels: [],
        datasets: [{
            label: '',
            data: [],
            backgroundColor: 'rgba(54, 162, 235, 0.2)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1.2
        }]
    },
    chartOptions: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    },
    percentages: [],
    selectableOptions : [
        { value: 'FB', label: 'Facebook' },
        { value: 'MSFT', label: 'Microsoft' },
        { value: 'GOOGL', label: 'Google'},
        { value: 'KO', label: 'Coca Cola'},
        { value: 'AAPL', label: 'Apple'}
    ],
    startDate: '2014-11-07',
    endDate: '2017-11-13',
    company: 'MSFT',
    width: 1000,
    height: 700
}

module.exports = Stock;