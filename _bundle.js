var React = require('react');
var createReactClass = require('create-react-class');
var LineChart = require("react-chartjs-2").Line;

function init()
{
    var apikey = "ZvxJtvoqPKVDNQz3CwRQ"
    var sDate = "2017-11-07"
    var eDate = "2017-11-10"
    var str = "https://www.quandl.com/api/v3/datasets/WIKI/FB/data.json?api_key="+
        apikey+"&start_date="+sDate+"&end_date="+eDate+"&column_index=4&order=asc"


    var chartData = {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    }
    var chartOptions = {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
}